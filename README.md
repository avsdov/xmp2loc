# xmp2loc

Iterates photos from the specified Wikimedia Commons category and 
extract XMP.drone-dji data from every photo. If Commons page is missing
{{Location}} template, the script will add the proper one.

You must specify CATEGORY, LOGIN and PASSWORD for Wikimedia Commons.
