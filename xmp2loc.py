#!/usr/bin/env python3

'''
    Iterates photos from the specified Wikimedia Commons category and.
    extract XMP.drone-dji data from every photo. If Commons page is missing
    {{Location}} template, the script will add the proper one.

    You must specify CATEGORY, LOGIN and PASSWORD for Wikimedia Commons.

    Option --use-yaw-as-direction may be specified in command line to specify 
    gimbal yaw as heading parameter of the {{Location}} template.

    @author Alexei Soloviev

    This program is free software; you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the Free Software
    Foundation; either version 2 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
    PARTICULAR PURPOSE. See the GNU General Public License for more details.
'''

import re
import requests
import json
import sys
from time import sleep
from time import time
from urllib.parse import quote_plus

# Commons auth data (DON'T FORGET TO SPECIFY YOURS):
LOGIN = None
PASSWORD = None
CATEGORY = 'Category:Aerial photographs by XXX'

BASE_URL = "https://commons.wikimedia.org/w/api.php"
USE_YAW = False
for opt in sys.argv:
    if opt == '--use-yaw-as-direction':
        USE_YAW = True
        break

def load_photo(filename):
    req1 = requests.get(BASE_URL+"?action=query&format=json&prop=wbentityusage|imageinfo|revisions&titles="+quote_plus(filename)+"&iiprop=url|extmetadata")
    info = json.loads(req1.content.decode())
    pageinfo = list(info['query']['pages'].values())[0]
    entity = list(pageinfo['wbentityusage'].keys())[0]
    url = pageinfo['imageinfo'][0]['url']

    if 'GPSLatitude' in pageinfo['imageinfo'][0]['extmetadata']:
        print("Coords already specified: %s,%s" % (pageinfo['imageinfo'][0]['extmetadata']['GPSLatitude']['value'], pageinfo['imageinfo'][0]['extmetadata']['GPSLongitude']['value']), flush=True)
        return None

    req2 = requests.get(url)
    res1 = re.search(rb'drone-dji:Latitude="([^"]+)"', req2.content)
    if not res1:
        res1 = re.search(rb'drone-dji:GpsLatitude="([^"]+)"', req2.content)
    if not res1:
        print("Latitude not found!", flush=True)
        return None
    lat = res1.group(1).decode()
    res2 = re.search(rb'drone-dji:Longitude="([^"]+)"', req2.content)
    if not res2:
        res2 = re.search(rb'drone-dji:GpsLongitude="([^"]+)"', req2.content)
    if not res2:
        print("Longitude not found!", flush=True);
        return None
    lon = res2.group(1).decode()
    res3 = re.search(rb'drone-dji:GimbalYawDegree="([^"]+)"', req2.content)
    yaw = None
    if res3:
        yaw = int(float(res3.group(1).decode()))
        if yaw < 0:
            yaw = yaw + 360
    return {'lat':lat, 'lon':lon, 'yaw': yaw, 'ts': pageinfo['revisions'][0]['timestamp'], 'rvid': pageinfo['revisions'][0]['revid'], 'wb': entity}


def edit_location(filename, coords):
    req1 = requests.get(BASE_URL+"?action=parse&format=json&prop=wikitext&page="+quote_plus(filename))
    info = json.loads(req1.content.decode())
    if 'wikitext' not in info['parse']:
        return None
    wikitext = info['parse']['wikitext']['*']
    res1 = re.search(r'\{\{location\|', wikitext, re.I)
    if res1:
        print('location already specified', flush=True)
        return None
    req2 = requests.get(BASE_URL+"?action=parse&format=json&prop=wikitext&section=1&page="+filename)
    info = json.loads(req2.content.decode())
    yaw = ''
    if USE_YAW and coords['yaw'] is not None:
        yaw = '|heading:%d' % coords['yaw']
    if abs(float(coords['lat']))<0.000001 or abs(float(coords['lon']))<0.000001:
        print('invalid coordinates', flush=True)
        return None
    return info['parse']['wikitext']['*'].strip() + ('\n{{Location|%s|%s%s}}\n' % (coords['lat'], coords['lon'], yaw))


if not LOGIN or not PASSWORD:
    print('LOGIN and PASSWORD required!', flush=True);
    sys.exit(1)

s = requests.Session()
#query auth manager
resp = s.get(BASE_URL+"?action=query&format=json&meta=authmanagerinfo&amirequestsfor=login")
#obtain LOGIN-token
resp = s.get(BASE_URL+"?action=query&format=json&meta=tokens&type=login")
login_resp = resp.json()
if 'tokens' in login_resp['query'] and 'logintoken' in login_resp['query']['tokens']:
    login_token = login_resp['query']['tokens']['logintoken']
else:
    print("Cannot obtain login token: "+resp.text, flush=True)
    sys.exit(7)
#auth
resp = s.post(BASE_URL+"?action=clientlogin&format=json", data={"username":LOGIN, "password":PASSWORD, "logintoken":login_token, "rememberMe":"1", "loginreturnurl":BASE_URL})
auth_resp = resp.json()
if 'status' in auth_resp['clientlogin'] and auth_resp['clientlogin']['status'] == 'PASS':
    pass
else:
    print("Login failed: "+resp.text, flush=True)
    sys.exit(8)
# obtain CSRF-token
resp = s.get(BASE_URL+"?action=query&format=json&meta=tokens")
csrf_resp = resp.json()
if 'tokens' in csrf_resp['query'] and 'csrftoken' in csrf_resp['query']['tokens']:
    csrf_token = csrf_resp['query']['tokens']['csrftoken']
else:
    print("Cannot obtain CSRF token: "+resp.text, flush=True)
    sys.exit(9)
print(csrf_token)


cmcontinue = ''
last_ts = time()

while True:
    req_cat = requests.get(BASE_URL+"?action=query&format=json&list=categorymembers&cmlimit=10&cmtitle="+CATEGORY+cmcontinue);
    info_cat = json.loads(req_cat.content.decode())
#    print(info_cat)

    if not info_cat['query']['categorymembers']:
        print("Search category failed!", flush=True)
        sys.exit(2)

    for item in info_cat['query']['categorymembers']:
        print(item['title'], flush=True)
        if item['title'].endswith('webm'):
            continue
        coords = load_photo(item['title'])
        if not coords:
            print('skip')
        else:
            print(coords)
            wikitext = edit_location(item['title'], coords)
            if wikitext:
                if time()-last_ts < 5:
                    sleep(time()-last_ts)
                resp = s.post(BASE_URL+"?action=edit&format=json",
                    data={"title": item['title'], "token": csrf_token, "section": 1,
                    "basetimestamp": coords['ts'], "bot": 1,
                    "summary": "extract coordinates from XMP.drone-dji", "text": wikitext})
                result = resp.json()
                print(result)
                if 'result' in result['edit'] and result['edit']['result'] == 'Success':
                    resp = s.post(BASE_URL+"?action=wbcreateclaim&format=json",
                        data={"entity": coords['wb'], "token": csrf_token, "snaktype": "value", "property": "P1259",
                        "value": json.dumps({'latitude': float(coords['lat']), 'longitude': float(coords['lon']), 'precision': 0.000001, 'globe': 'http://www.wikidata.org/entity/Q2'}),
                        "baserevid": coords['rvid'], "bot": 1,
                        "summary": "add coordinates to structured data"})
                    result = resp.json()
                    print(result)
                    print("ok")
                else:
                    print("Edit failed: "+resp.text)
                last_ts = time()

#stop after 10 photos, remove next line to process the whole category
#    sys.exit(0)

    if 'continue' not in info_cat or not info_cat['continue']['cmcontinue']:
        break
    cmcontinue = "&cmcontinue="+info_cat['continue']['cmcontinue']

print('All done, have fun!', flush=True);
